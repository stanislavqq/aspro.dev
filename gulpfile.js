const {series, watch, parallel, src, dest} = require('gulp');
const minifyCss = require('gulp-minify-css');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');

const paths = {
    css: [
        './css/bootstrap.min.css',
        './css/app.css'
    ],
    js: [
        './js/jquery-3.5.1.min.js',
        './js/bootstrap.min.js',
        './js/jquery.sticky.js',
        './js/mobileNav.js',
        './js/app.js',
    ],
    images: './images/*',
    dest: './',
}

function clean(cb) {
    // body omitted
    cb();
}

function css(cb) {
    return src(paths.css)
        .pipe(minifyCss())
        .pipe(concat('app.min.css'))
        .pipe(dest(paths.dest + '/css'));
    cb();
}

function javascript(cb) {
    return src(paths.js)
        .pipe(concat('bundle.js'))
        .pipe(dest(paths.dest + '/js'))
        .pipe(minify({
            ext: {
                min: '.min.js'
            },
            ignoreFiles: ['-min.js', '.min.js']
        }))
        .pipe(dest(paths.dest + '/js'));

    cb();
}

function images(cb) {
    return src(paths.images)
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(dest(paths.dest + '/images'));

    cb();
}

exports.watch = function () {
    watch('css/app.less', {queue: false, delay: 500}, css);
    watch(paths.js, {queue: false, delay: 500}, javascript);
};
exports.default = series(clean, parallel(css, javascript), images);
