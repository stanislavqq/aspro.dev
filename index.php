<?php

$data = require('templates/data.php');
$isFront = basename(__FILE__) === 'index.php'; //$_SERVER['REQUEST_URI'] === '/';
require 'layouts/header.php';
?>

    <main>
        <section class="unified-platform">
            <div class="container custom-container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 content">
                        <h1 class="title text-center text-lg-left">
                            Единая платформа, объединяющая модули <br>
                            для успешного бизнеса
                        </h1>

                        <div class="description text-center text-lg-left">
                            Наши последние разработки объединили в себе мощный функционал, последние технологии в сфере
                            электронной коммерции и стильный дизайн
                        </div>

                        <form action="" method="GET" name="get-release-form" class="form-get-release m-auto m-lg-0">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" placeholder="Введите E-mail"
                                       aria-label="Введите E-mail" aria-describedby="basic-addon" required>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Узнать о запуске</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-6 d-none d-lg-block image">
                        <figure>
                            <img class="img-fluid" src="images/promo-image.png"
                                 alt="Единая платформа, объединяющая модули для успешного бизнеса"
                                 title="Единая платформа, объединяющая модули для успешного бизнеса">
                        </figure>
                    </div>
                </div>
            </div>
        </section>

        <?php
        $cases = [
            [
                'title' => 'Финансы',
                'desc' => 'Учет финансов и движения денежных потоков компании',
                'image' => 'feature-icon-1.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'Проекты',
                'desc' => 'Ведите проекты, учитывайте затраты и платежи по проекту',
                'image' => 'feature-icon-2.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'Задачи',
                'desc' => 'Удобный таск-менеджер значительно повысит показатели',
                'image' => 'feature-icon-3.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'KPI',
                'desc' => 'Измерение достижения целей для отделов и компании в целом',
                'image' => 'feature-icon-4.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'CRM',
                'desc' => 'Cистема управления взаимоотношениями с клиентами',
                'image' => 'feature-icon-5.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'Знания',
                'desc' => 'Накопленный опыт компании в одном месте',
                'image' => 'feature-icon-6.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'Идея',
                'desc' => 'Делиться идеями и инновациями стало еще проще',
                'image' => 'feature-icon-7.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'HR',
                'desc' => 'Модуль управления человеческими ресурсами',
                'image' => 'feature-icon-8.svg',
                'url' => '/accounting.php'
            ],
            [
                'title' => 'Service',
                'desc' => 'Техническая поддержка через тикетную систему',
                'image' => 'feature-icon-9.svg',
                'url' => '/accounting.php'
            ]
        ];
        ?>

        <div class="feature-list">
            <div class="container-lg custom-container">
                <div class="row">
                    <?php foreach ($cases as $index => $case): ?>
                        <div class="col-lg-4 col-md-6">
                            <a class="feature-item" href="<?= $case['url'] ?>" title="<?= $case['title'] ?>">
                                <div class="item-content">
                                    <div class="title"><?= $case['title'] ?></div>
                                    <div class="description"><?= $case['desc'] ?></div>
                                </div>
                                <div class="icon">
                                    <img src="/images/<?= $case['image'] ?>" alt="<?= $case['title'] ?>">
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>
<?php require 'layouts/footer.php'; ?>