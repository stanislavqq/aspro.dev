<?php
if (!function_exists('renderThreeMenu')) {
	/**
	 * Class MenuItem
	 *
	 * @property integer $id
	 * @property integer $parent
	 * @property string $title
	 * @property string $url
	 * @property MenuItem[] $items
	 */
	class MenuItem
	{
		/* @var static $counter */
		private static $counter = 0;

		/* @var integer $id */
		public $id;

		/* @var string $title */
		public $title;

		/* @var string $id */
		public $url;

		/* @var integer $parent */
		public $parent = null;

		/* @var MenuItem[] $items */
		public $items = null;

		/**
		 * MenuItem constructor.
		 *
		 * @param string $title
		 * @param string $url optional
		 * @param array $items optional
		 */
		public function __construct(string $title, string $url = null, $items = null)
		{
			$this->id = ++static::$counter;
			$this->title = $title;
			$this->url = $url;

			if ($items) {
				$this->setParentIds($items);
				$this->items = $items;
			}
		}

		/**
		 * @param MenuItem[] $items
		 */
		private function setParentIds(array $items)
		{
			foreach ($items as &$item) {
				$item->parent = $this->id;
			}
		}
	}


	/**
	 * @param MenuItem[] $items
	 *
	 * @param bool $recursive
	 *
	 * @return string
	 */
	function renderThreeMenu(array $items, bool $recursive = true)
	{
		$result = '';

		if ($items[0]->parent) {
			$result .= '<ul class="dropdown-menu" data-parent="' . $items[0]->parent . '">';
		} else {
			$result .= '<ul class="dropdown-menu">';
		}

		foreach ($items as $index => $item) {
			$hasItems = (bool) $item->items;

			if ($hasItems) {
				$result .= '<li class="dropdown dropright">';
			} else {
				$result .= '<li>';
			}

			if ($hasItems) {
				$result .= '<a id="' . $item->id . '" class="dropdown-item dropdown-toggle"  href="' . $item->url . '"
                   data-toggle="dropdown" 
                   aria-haspopup="true" 
                   aria-expanded="false">';
			} else {
				$result .= '<a id="' . $item->id . '" class="dropdown-item" href="' . $item->url . '" title="' . $item->title . '">';
			}

			$result .= $item->title;
			$result .= '</a>';

			if ($hasItems && $recursive) {
				$result .= renderThreeMenu($item->items);
			}

			$result .= '</li>';
		}
		$result .= '</ul>';

		return $result;
	}

}


$data['menu'] = [
	new MenuItem('Облачная CRM', '#',
		[
			new MenuItem('Возможности', '#'),
			new MenuItem('Решения', '#',
				[
					new MenuItem('По размеру', '#'),
					new MenuItem('По типу использования', '#',
						[
							new MenuItem('Автоматизация работы с клиентами', '#'),
							new MenuItem('KPI и цели', '#'),
							new MenuItem('Автоматизация документов оборота', '#'),
							new MenuItem('Обучение и HR', '#'),
						]
					),
					new MenuItem('По отраслям', '#'),
				]
			),
			new MenuItem('Интеграции', '#'),
			new MenuItem('Внедрения', '#'),
			new MenuItem('Цены', '#'),
			new MenuItem('Поддержка', '#'),
		]
	),
	new MenuItem('Готовые сайты', '#',
		[
			new MenuItem('Интернет-магазины', '#'),
			new MenuItem('Корпоративные сайты', '#'),
			new MenuItem('Магазин', '#'),
			new MenuItem('Услуги', '#'),
			new MenuItem('Портфолио', '#'),
			new MenuItem('Поддержка', '#'),
		]
	),
	new MenuItem('Блог', '#'),
	new MenuItem('Партнерам', '#'),
	new MenuItem('Компания', '#'),
];

return $data;