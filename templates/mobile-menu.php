<?php include_once 'data.php' ?>

<ul class="navbar-nav mx-auto">

    <?php
    $items = [];
    /**
     * @var MenuItem $menuItem
     */
    foreach ($data['menu'] as $index => $menuItem):
        $hasChildItems = (bool)$menuItem->items;

        if ($hasChildItems) {
            $items[] = renderThreeMenu($menuItem->items, false);
        }

        ?>
        <li class="nav-item <?= $hasChildItems ? 'dropdown' : '' ?>">
            <a class="nav-link <?= $hasChildItems ? 'dropdown-toggle' : '' ?>"
               href="<?= $menuItem->url ?>"
               title="<?= $menuItem->title ?>">
                <?= $menuItem->title ?>
            </a>
            <?php
            if ($hasChildItems) {
                print renderThreeMenu($menuItem->items);
            }
            ?>
        </li>
    <?php endforeach; ?>
</ul>