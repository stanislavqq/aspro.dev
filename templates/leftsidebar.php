<nav class="left-sidebar-nav">
    <button class="btn btn-mobile-sidebar-toggler dropdown-toggle" type="button">Меню</button>
    <ul class="nav flex-column">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" title="Управленческий учет">Управленческий учет</a>
            <ul class="dropdown-menu">
                <li class="">
                    <a href="#" title="Введение в управленческий учет в IT-бизнесе">Введение в управленческий учет в
                        IT-бизнесе <span class="drop-arrow"></span></a>
                </li>
                <li class="dropdown active">
                    <a class="dropdown-toggle active" href="#" title="Система управленческого учета">Система управленческого учета</a>
                    <ul class="dropdown-menu">
                        <li><a class="active" href="#" title="Организация системы управленческого учета">Организация системы
                                управленческого учета</a></li>
                        <li><a href="#" title="Введение в управленческий учет">Введение в управленческий учет</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#" title="Часто задаваемые вопросы (FAQ)">Часто задаваемые вопросы (FAQ)</a>
                </li>
                <li>
                    <a href="#" title="Глоссарий">Глоссарий</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" title="Бухгалтерский учет">Бухгалтерский учет</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" title="Финансовый учет">Финансовый учет</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" title="Управленческий учет">Управленческий учет</a>
        </li>
    </ul>
</nav>
