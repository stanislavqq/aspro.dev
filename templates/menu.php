<ul class="navbar-nav mx-auto">

	<?php
	/**
	 * @var MenuItem $menuItem
	 */
    foreach ($data['menu'] as $index => $menuItem):
	$hasChildItems = (bool)$menuItem->items;

	?>
    <li class="nav-item <?= $hasChildItems ? 'dropdown' : '' ?>">
        <a class="nav-link <?= $hasChildItems ? 'dropdown-toggle' : '' ?>"
           href="<?= $menuItem->url ?>"
           id="<?= $menuItem->id ?>"
           <?php if ($hasChildItems): ?>
           data-toggle="dropdown"
           aria-haspopup="true"
           aria-expanded="false"
           <?php endif; ?>
           title="<?= $menuItem->title ?>">
            <?= $menuItem->title ?>
        </a>
        <?php
        if ($hasChildItems) {
            print renderThreeMenu($menuItem->items);
        }
        ?>
    </li>
    <?php endforeach; ?>
</ul>