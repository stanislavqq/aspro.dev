<?php
define("GENERATE", true);
$scan = scandir(__DIR__);

$excludedFileNames = ['generate'];
$toExt = 'html';
$distDirName = 'public';
$concurrentDirectory = __DIR__ . DIRECTORY_SEPARATOR . $distDirName;
if (!is_dir($concurrentDirectory) && !mkdir($concurrentDirectory)) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

foreach (array_diff($scan, ['.', '..']) as $item) {
	$exploded = explode('.', $item);
	$fileName = array_shift($exploded);
	$ext = array_pop($exploded);

	if ($ext === 'php' && !in_array($fileName, $excludedFileNames, true)) {
		ob_start();
		require_once $item;
		$content = ob_get_clean();
		$fileHandle = fopen($distDirName . '/' . $fileName . '.' . $toExt, 'w');
		$res = fwrite($fileHandle, $content);
		if ($res) {
			print $item . ' ' . 'OK <br>';
		} else {
			print $item . ' ' . 'NOT OK <br>';
		}
		ob_end_clean();
	}
}
