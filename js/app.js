//MAIN
$(document).ready(function () {

    headerBgScrollHandler(); //Calling when page load is first
    $(document).scroll(headerBgScrollHandler);

    $('#mobile-nav').mobileNav();

    $('.footer-menu a.title-link').click(function (e) {
        let content = $(this).next('ul');
        if (content.length && $(window).width() < 991) {
            e.preventDefault();
            $('.menu-list-title + ul').slideUp();
            content.slideToggle();
        }
    });

    $('.left-sidebar-nav .dropdown-toggle').click(function (e) {
        e.preventDefault();
        $(this).next('.dropdown-menu').slideToggle();
    });

    $('.btn-mobile-sidebar-toggler.dropdown-toggle').click(function (e) {
        $('.left-sidebar-nav>ul').slideToggle();
    });

    if ($('.left-sidebar').length) {
        $('.left-sidebar').sticky({
            stickyOffsetTop: 140,
            stickyOffsetBottom: 20,
        });
    }
});

/**
 * Set class bg-white to app header when scroll value > 0
 */
function headerBgScrollHandler() {
    let $header = $('header');
    let scrollTop = $(this).scrollTop();
    let $knowAboutBtn = $('#know-about-release-btn');
    let hasBgWhite = $header.is('.bg-white');

    if (scrollTop > 50 && !hasBgWhite) {
        $header.addClass('bg-white');

        $knowAboutBtn.removeClass('btn-light');
        $knowAboutBtn.addClass('btn-primary');

    } else if (scrollTop === 0 && hasBgWhite) {
        $header.removeClass('bg-white');

        $knowAboutBtn.removeClass('btn-primary');
        $knowAboutBtn.addClass('btn-light');
    }

    return $(this);
}
