$.fn.mobileNav = function (options) {
    let self = this;

    let settings = $.extend({
        showClass: 'show',
        popover: $('.mobile-nav-popover'),
        menuBack: '.menu-back',
        menuContainer: '.mobile-nav-menu',
        preloadImgUrl: '/images/preloader.svg'
    }, options);

    let stageDepthCounter = 0;
    let popover = settings.popover;
    let showClass = settings.showClass;
    let menuBack = $(settings.menuBack);
    let preloaderImg = $('<img>').attr('src', settings.preloadImgUrl);
    let preloader = $("<div class='preload'></div>");
    preloader.append(preloaderImg)
    init();

    return this;

    function init() {

        initEvents();

        let pxToSwipe = 70;
        let swipeStart = 0;
        let $nav = $('.mobile-nav-menu');


        self
            .on('touchstart', function (e) {
                swipeStart = e.touches[0].pageX;
            })
            .on('touchend', function (e) {
                let x = e.changedTouches[0].pageX;

                if (x <= (swipeStart - pxToSwipe)) {
                    hide();
                }
            })
            .on('click', '.dropdown-toggle', function (e) {
                e.preventDefault();

                let parentRow = $(this).closest('li');
                let subMenu = parentRow.children('.dropdown-menu');
                let menuWidth = parseFloat($nav.width());
                let $navList = $('.mobile-nav-menu>ul');

                if (stageDepthCounter === 0) {
                    $('#mobile-nav .logo').css('transform', 'translateX(-150%)');
                    menuBack.show();
                }

                stageDepthCounter++;

                console.log(subMenu, $navList);
                subMenu.attr('data-stage', stageDepthCounter);
                subMenu.css('display', 'block');

                $navList.css('left', (stageDepthCounter * menuWidth * -1) + 'px');
            })
            .on('click', settings.menuBack, function () {
                let menuWidth = parseFloat($nav.width());
                let $navList = $('.mobile-nav-menu>ul');

                if (stageDepthCounter <= 0) return false;

                let $curMenu = $('.mobile-nav-menu .dropdown-menu[data-stage="' + stageDepthCounter + '"]');
                console.log($curMenu);
                $curMenu.slideUp('fade');
                stageDepthCounter--;

                if (stageDepthCounter === 0) {
                    $('#mobile-nav .logo').css('transform', '');
                    $(this).hide();
                }

                $navList.css('left', (stageDepthCounter * menuWidth * -1) + 'px');
            })

    }

    function initEvents() {
        $('.navbar-toggler').click(function (e) {
            e.preventDefault();

            show();
            loadMenu();
        });

        $('#mobile-nav button.close').click(function (e) {
            e.preventDefault();

            hide();
        });

        popover.click(function () {
            hide();
        })
    }

    function loadMenu() {
        let mobNav = $(settings.menuContainer);

        $.ajax({
            type: 'GET',
            url: '/templates/mobile-menu.php',
            beforeSend: function () {
                mobNav.html(preloader.html());
            },
            success: function (response) {
                mobNav.html(response);
            },
            complete: function () {
                if (mobNav.children('.preload').length) {
                    mobNav.children('.preload').remove();
                }
            },
            error: function (error) {
                mobNav.html('');
            }
        });
    }

    function show() {
        popover.addClass('show');
        self.addClass(showClass);
        $('body').css('overflow-y', 'hidden');
    }

    function hide() {
        popover.removeClass('show');
        self.removeClass(showClass);
        $('body').css('overflow-y', '');

        self.stageDepthCounter = 0;
        $('.mobile-nav-menu ul.navbar-nav').css('left', 0);
        $('#mobile-nav .logo').css('transform', '');
        menuBack.hide();
        $('.mobile-nav-menu .dropdown-menu').hide();
    }

    function toggle() {
        if (self.hasClass(showClass))
            hide();
        else
            show();
    }
}


