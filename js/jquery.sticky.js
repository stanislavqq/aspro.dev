/**
 *
 */

(function ($) {
    let debug = false;

    let savePos,
        direction,
        self,
        elInfo,
        stage,
        elementStyles;

    let settings = {}

    $.fn.sticky = function (options) {
        self = this;
        settings = $.extend({
            parent: self.closest('div'),
            stickyOffsetTop: 60,
            stickyOffsetBottom: 100,
            debug: false,
        }, options)

        debug = settings.debug;

        elementStyles = {
            position: 'static',
            top: 0
        };

        self.css('width', settings.parent.width());
        $(window).resize(function () {
            self.css('width', settings.parent.width());
        });

        elInfo = {
            width: self.width(),
            height: self.height(),
            position: {
                top: self.offset().top,
                bottom: settings.parent.offset().top + self.height()
            }
        }

        settings.parent.css('position', 'relative');
        scrollHandler();
        $(window).scroll(scrollHandler);
    };

    function scrollHandler(event) {
        let windowHeight = $(this).height();


        let windowScroll = {
            top: $(this).scrollTop(),
            bottom: $(this).scrollTop() + windowHeight
        };

        if ($(window).height() >= self.height() + settings.stickyOffsetTop) {
            self.css({
                position: 'sticky',
                top: settings.stickyOffsetTop
            });
            return self;
        }

        if ((stage === 'top' && direction === 'down') || (stage === 'bottom' && direction === 'up')) {
            setStickyStage('freeze');
        }

        if (stage === 'freeze' && direction === 'down' && windowScroll.bottom >= (self.offset().top + self.height() + settings.stickyOffsetBottom)) {
            setStickyStage('bottom');
        }

        if (stage === 'freeze' && direction === 'up' && windowScroll.top <= (self.offset().top - settings.stickyOffsetTop)) {
            setStickyStage('top');
        }


        if (direction === 'down' && windowScroll.bottom >= elInfo.position.bottom && stage !== 'end' && stage !== 'freeze') {
            setStickyStage('bottom');
        }

        if (direction === 'down' && (windowScroll.bottom) >= (settings.parent.offset().top + settings.parent.height()) && stage !== 'end') {
            setStickyStage('end');
        }

        if (direction === 'up' && (windowScroll.top + settings.stickyOffsetTop) <= (settings.parent.offset().top + settings.parent.height() - self.height()) && stage !== 'freeze') {
            setStickyStage('top');
        }

        if (direction === 'up' && (windowScroll.top + settings.stickyOffsetTop) <= settings.parent.offset().top && stage !== 'start') {
            setStickyStage('start');
        }

        if (windowScroll.top >= savePos) {
            direction = 'down';
        } else {
            direction = 'up';
        }

        savePos = windowScroll.top;

        if (debug) {
            console.log('direction', direction);
        }

        self.css(elementStyles);
    }

    function setStickyStage(stringStage) {
        switch (stringStage) {
            case 'top':
                self.attr('data-stage', 'top');
                stage = 'top';
                elementStyles = {
                    position: 'fixed',
                    top: settings.stickyOffsetTop
                }
                break;
            case 'bottom':
                self.attr('data-stage', 'bottom');
                stage = 'bottom';
                elementStyles = {
                    position: 'fixed',
                    top: ($(window).height() - self.height()) - settings.stickyOffsetBottom
                }
                break;

            case 'start':
                self.attr('data-stage', 'start');
                stage = 'start';
                elementStyles = {
                    position: 'absolute',
                    top: 0
                }
                break;
            case 'end':
                self.attr('data-stage', 'end');
                stage = 'end';
                elementStyles = {
                    position: 'absolute',
                    top: settings.parent.height() - self.height()
                }
                break;
            case 'freeze':
                self.attr('data-stage', 'freeze');
                stage = 'freeze';
                elementStyles = {
                    position: 'absolute',
                    top: self.offset().top - settings.parent.offset().top
                }
                break;
        }

        return stage;
    }

})(jQuery)