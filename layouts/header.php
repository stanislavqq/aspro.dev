<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Аспро.Cloud</title>
    <meta name="description" content="Единая платформа, объединяющая модули для успешного бизнеса">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="icon" type="image/png" href="/favicon.png">
</head>
<body <?= $isFront ? 'class="bg-purple"' : '' ?>>
<div id="app">
    <div class="mobile-nav-popover"></div>

    <aside id="mobile-nav" class="mobile-nav d-block d-md-none">
        <div class="container-fluid logo-wrapper">
            <button class="menu-back" type="button" title="Назад">
                <span>&leftarrow;</span>
            </button>
            <a class="logo mx-auto" href="/" title="На главную">
                <img src="./images/logo.png" alt="Логотип Аспро.Cloud"
                     title="Аспро.Cloud">
                <span>Аспро.Cloud</span>
            </a>
            <button type="button" class="close" title="Закрыть">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <nav class="mobile-nav-menu"></nav>
    </aside>

    <header class="app-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <nav id="app-menu" class="navbar justify-content-center navbar-expand-md align-baseline">
                        <a class="navbar-brand logo mx-auto" href="/" title="На главную">
                            <img src="./images/logo.png"
                                 alt="Логотип Аспро.Cloud"
                                 title="Аспро.Cloud">
                            <span>Аспро.Cloud</span>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavAltMarkup"
                                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <?php require 'templates/menu.php' ?>
                        </div>
                        <form class="form-inline d-none d-lg-block">
                            <button data-toggle="modal" data-target="#know-about-release" id="know-about-release-btn"
                                    class="header-button-right btn <?= $isFront ? 'btn-light' : 'btn-primary' ?> btn-rounded" type="button"
                                    title="Узнать о запуске">
                                Узнать о запуске
                                <span>×</span>
                            </button>
                        </form>
                    </nav>

                </div>
            </div>
        </div>
    </header>
